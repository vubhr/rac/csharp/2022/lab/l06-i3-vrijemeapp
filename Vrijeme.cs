﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VrijemeApp {
  public class Vrijeme {
    public Vrijeme() {
      podaci = DohvatiPodatke();
      // Thread osvjezavanje = new Thread(OsvjeziPodatke);
      // osvjezavanje.IsBackground = true;
      // osvjezavanje.Start();
    }

    public void OsvjeziPodatke() {
      //while (true) {
        //Thread.Sleep(300000); // 300.000 ms = 3000s = 5min
        podaci = DohvatiPodatke();
      //}
    }

    public List<VrijemePodatak> DohvatiPodatke() {
      XmlDocument vrijeme = new XmlDocument();
      vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");

      List<VrijemePodatak> podaci = new List<VrijemePodatak>();

      XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
      foreach (XmlNode grad in gradovi) {
        podaci.Add(new VrijemePodatak(
            grad["GradIme"].InnerText,
            grad["Podatci"]["Temp"].InnerText,
            grad["Podatci"]["Vlaga"].InnerText,
            grad["Podatci"]["Tlak"].InnerText
        ));
      }
      return podaci;
    }

    public IEnumerable<string> DohvatiGradove() {
      return
        from p in podaci
        orderby p.Grad
        select p.Grad;
    }

    public VrijemePodatak DohvatiPodatkeZaGrad(string grad) {
      return
        (from p in podaci
         where p.Grad == grad
         select p).First();
    }

    public IEnumerable<VrijemePodatak> DohvatiNajtoplijeGradove(int kolicina) {
      return
        (from p in podaci
         orderby p.Temperatura descending
         select p).Take(kolicina);
    }

    public IEnumerable<VrijemePodatak> DohvatiNajhladnijeGradove(int kolicina) {
      return
        (from p in podaci
         orderby p.Temperatura
         select p).Take(kolicina);
    }

    private List<VrijemePodatak>? podaci;
  }
}
