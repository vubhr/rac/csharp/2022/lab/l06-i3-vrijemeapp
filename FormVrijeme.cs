namespace VrijemeApp {
  public partial class FormVrijeme : Form {
    private Vrijeme vrijeme;

    public FormVrijeme() {
      InitializeComponent();
      vrijeme = new Vrijeme();

      OsvjeziPopisGradova();
      OsvjeziNajhladnijeINajtoplijeGradove();
    }

    private void OsvjeziPopisGradova() {
      cbGrad.Items.Clear();
      cbGrad.Items.AddRange(vrijeme.DohvatiGradove().ToArray());
    }

    private void PrikaziPodatkeZaGrad(string grad) {
      var podaci = vrijeme.DohvatiPodatkeZaGrad(grad);

      lblGrad.Text = podaci.Grad;
      lblTemperatura.Text = podaci.TemperaturaZaPrikaz;
      lblVlaga.Text = podaci.VlagaZaPrikaz;
      lblTlak.Text = podaci.TlakZaPrikaz;
    }

    private void OsvjeziNajhladnijeINajtoplijeGradove() {
      lbNajtopliji.Items.Clear();
      lbNajhladniji.Items.Clear();

      lbNajtopliji.Items.AddRange(vrijeme.DohvatiNajtoplijeGradove(5).ToArray());
      lbNajhladniji.Items.AddRange(vrijeme.DohvatiNajhladnijeGradove(5).ToArray());
    }

    private void cbGrad_SelectedIndexChanged(object sender, EventArgs e) {
      PrikaziPodatkeZaGrad(cbGrad.Text);
    }
  }
}